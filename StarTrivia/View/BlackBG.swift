//
//  BlackBG.swift
//  StarTrivia
//
//  Created by rafiul Hasan on 23/6/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class BlackBGView: UIView {
    override func awakeFromNib() {
        layer.backgroundColor = black_BG
        layer.cornerRadius = 10
    }
}

class BlackBGButton: UIButton {
    override func awakeFromNib() {
        layer.backgroundColor = black_BG
        layer.cornerRadius = 10
    }
}

