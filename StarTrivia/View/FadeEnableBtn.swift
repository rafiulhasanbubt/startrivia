//
//  FadeEnableBtn.swift
//  StarTrivia
//
//  Created by rafiul Hasan on 24/6/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class FadeEnabledBtn: UIButton {
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                UIView.animate(withDuration: 0.2) {
                    self.alpha = 1.0
                }
            } else {
                UIView.animate(withDuration: 0.2) {
                    self.alpha = 0.5
                }
            }
        }
    }
}
