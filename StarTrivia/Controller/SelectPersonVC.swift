//
//  ViewController.swift
//  StarTrivia
//
//  Created by rafiul Hasan on 23/6/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

protocol PersonProtocol {
    var person: Person! {get set}
}

class SelectPersonVC: UIViewController {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var massLbl: UILabel!
    @IBOutlet weak var hairLbl: UILabel!
    @IBOutlet weak var birthYearLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    
    @IBOutlet weak var homeworldBtn: UIButton!
    @IBOutlet weak var vehiclesBtn: UIButton!
    @IBOutlet weak var starshipsBtn: UIButton!
    @IBOutlet weak var filmsBtn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var personApi = PersonAPI()
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func randomClicked(_ sender: Any) {
        let random = Int.random(in: 1 ... 87)
        spinner.startAnimating()
        personApi.getRandomPersonAlamoCodable(id: random) { (person) in
            self.spinner.stopAnimating()
            if let person = person {
                self.setupView(person: person)
                self.person = person
            }
        }
    }
    
    func setupView(person: Person) {
        nameLbl.text = person.name
        heightLbl.text = person.height
        massLbl.text = person.mass
        hairLbl.text = person.hair
        birthYearLbl.text = person.birthYear
        genderLbl.text = person.gender
        
        // beginner way
//        if person.homeworldUrl.isEmpty {
//            homeworldBtn.isEnabled = false
//        } else {
//            homeworldBtn.isEnabled = true
//        }
//        if person.vehicleUrls.isEmpty {
//            vehiclesBtn.isEnabled = false
//        } else {
//            vehiclesBtn.isEnabled = true
//        }
//        if person.starshipUrls.isEmpty {
//            starshipsBtn.isEnabled = false
//        } else {
//            starshipsBtn.isEnabled = true
//        }
//        if person.filmUrls.isEmpty {
//            filmsBtn.isEnabled = false
//        } else {
//            filmsBtn.isEnabled = true
//        }
        
        //expert way
        homeworldBtn.isEnabled = !person.homeworldUrl.isEmpty
        vehiclesBtn.isEnabled = !person.vehicleUrls.isEmpty
        starshipsBtn.isEnabled = !person.starshipUrls.isEmpty
        filmsBtn.isEnabled = !person.filmUrls.isEmpty
    }
    // remove button ibAction insteat of switch case
//    @IBAction func homeworldClicked(_ sender: Any) {
//    }
//
//    @IBAction func vehiclesClicked(_ sender: Any) {
//    }
//
//    @IBAction func starshipsClicked(_ sender: Any) {
//    }
//
//    @IBAction func filmsClicked(_ sender: Any) {
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // use protocol to send data
        if var destination = segue.destination as? PersonProtocol {
            destination.person = person
        }
        
        // using segue with identifier
//        if segue.identifier == "toHomeworld" {
//            if let destination = segue.destination as? HomeworldVC {
//                destination.person = person
//            }
//        }
//        else if segue.identifier == "toVehicles" {
//            if let destination = segue.destination as? VehiclesVC {
//                destination.person = person
//            }
//        } else if segue.identifier == "toStarships" {
//            if let destination = segue.destination as? StarshipsVC {
//                destination.person = person
//            }
//        } else if segue.identifier == "toFilms" {
//            if let destination = segue.destination as? FilmsVC {
//                destination.person = person
//            }
//        }
        
        // use switch case with identifier to send data
//        switch segue.identifier {
//        case Segue.homeworld.rawValue:
//            if let destination = segue.destination as? HomeworldVC {
//                destination.person = person
//            }
//        case Segue.vehicles.rawValue:
//            if let destination = segue.destination as? VehiclesVC {
//                destination.person = person
//            }
//        case Segue.starships.rawValue:
//            if let destination = segue.destination as? StarshipsVC {
//                destination.person = person
//            }
//        case Segue.films.rawValue:
//            if let destination = segue.destination as? FilmsVC {
//                destination.person = person
//            }
//        default:
//            break
//        }
    }
    // create enum to use switch case
//    enum Segue : String {
//        case homeworld = "toHomeworld"
//        case vehicles = "toVehicles"
//        case starships = "toStarships"
//        case films = "toFilms"
//    }
}

