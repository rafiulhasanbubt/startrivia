//
//  Homeworld.swift
//  StarTrivia
//
//  Created by rafiul Hasan on 23/6/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation

struct Homeworld: Codable {
    let name: String
    let climate: String
    let terrain: String
    let population: String
}
